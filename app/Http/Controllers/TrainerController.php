<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\trainer;
use App\Http\Requests\StoreTrainerRequest;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admin');      
        $trainers= trainer::all();
        return view('entrenadores.index',compact('trainers'));
      // return 'hola entraste en el trainers';
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
        return view('entrenadores.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrainerRequest $request)
    {

      // 
        //
      //  return  $request;
        if($request->hasFile('avatar')){
            $file=$request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/' , $name);
        }
        $trainer = new trainer();
        $trainer->name=$request->input('nombre');
        $trainer->avatar=$name;
        $trainer->description=$request->input('descripcion');
        $trainer->slug=$request->input('nombre');
        $trainer->save();
        return redirect()->route('entrenadores.index');
      //  return'guardado';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $trainer = trainer::where('slug','=',$slug)->firstOrFail();
       // return $trainer;
        return view('entrenadores.show',compact('trainer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $trainer = trainer::where('slug','=',$slug)->firstOrFail();
        // return $trainer;
        return view('entrenadores.edit',compact('trainer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $trainer = trainer::where('slug','=',$slug)->firstOrFail();
        $trainer->fill($request->except('avatar'));
        $trainer->save();
        return redirect()->route('entrenadores.show',[$trainer->slug]);
        //        return 'actualizado';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $trainer = trainer::where('slug','=',$slug)->firstOrFail();
        $file_path= public_path().'/images/'.$trainer->avatar;
        \File::delete($file_path);       
        $trainer->delete();
        return redirect()->route('entrenadores.index');
        //         return 'eliminado';
    }
}
