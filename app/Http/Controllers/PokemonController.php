<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\trainer;
use App\Pokemon;

class PokemonController extends Controller
{
    public function index($slug, Request $request){
      $trainer = trainer::where('slug','=',$slug)->firstOrFail();
        if($request->ajax()){
          $pokemons= $trainer->pokemons;
          return response()->json($pokemons,200);

        }
        return view('pokemons.index');
      // return 'hola entraste en el trainers';
      
        //
    }

    public function store($slug,Request $request){
      $trainer = trainer::where('slug','=',$slug)->firstOrFail();
     
      if($request->ajax()){
        $pokemon = new Pokemon();
        $pokemon->name= $request->input('name');
        $pokemon->picture=$request->input('picture');
        $pokemon->trainer()->associate($trainer)->save();
     //   $pokemon->save();

        return response()->json([
          "message"=>"pokemon creado correctamente",
         "pokemon"=>$pokemon
        ],200);
      }
    }

}
