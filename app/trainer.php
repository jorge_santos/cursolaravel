<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trainer extends Model
{
    protected $fillable = ['name', 'description'];

    public function pokemons (){
        return $this->hasMany('App\Pokemon');
    }
   
}
