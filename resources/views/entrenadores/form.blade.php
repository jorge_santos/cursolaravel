<div class="container">
        <form action="/entrenadores" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">

                <span class="tag is-white">Nombre del entrenador</span>
                <input class="input is-primary" name="nombre" type="text" placeholder="Nombre">

            </div>
            <div class="form-group">

              <span class="tag is-white">Avatar</span>
              <input name="avatar" type="file">

            </div>
            <div class="form-group">

                <span class="tag is-white">Descripcion del entrenador</span>
                <input class="input is-primary" name="descripcion" type="text">

            </div>
         <button class="button">Guardar</button>
        </form>

    </div>
