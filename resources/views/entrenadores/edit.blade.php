<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <title>EditarEntrenadores</title>
</head>

<body>

<nav class="navbar is-success" role="navigation" aria-label="main navigation">
        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item">
                    prueba
                </a>

                <a class="navbar-item">
                    Documentation
                </a>
            </div>

        </div>
    </nav>

    <div class="container">
        <form action="/entrenadores/{{$trainer->slug}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="level-item has-text-centered">
                <figure class='image is-128x128'>
                    <img style="background-color: #EFEFEF;" class="is-rounded" src="images/{{$trainer->avatar}}">
                </figure>
             </div>

            <div class="form-group">

                <span class="tag is-white">Nombre del entrenador</span>
                <input class="input is-primary" name="name" type="text" placeholder="Nombre" value="{{$trainer->name}}">

            </div>

            <div class="form-group">

                <span class="tag is-white">Descripcion del entrenador</span>
                <input class="input is-primary" name="description" type="text" value="{{$trainer->description}}">

            </div>
         <button class="button is-danger">Actualizar</button>
        </form>

    </div>

</body>

</html>