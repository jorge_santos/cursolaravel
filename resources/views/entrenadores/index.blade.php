<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <title>index</title>
</head>

<body>

    <nav class="navbar is-success" role="navigation" aria-label="main navigation">
        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item">
                    prueba
                </a>

                <a class="navbar-item">
                    Documentation
                </a>
            </div>

        </div>
    </nav>



    @foreach ($trainers as $entrenador)
    <div class="row">
        <div class="container">
            <div class='column is-one-quarter has-text-centered'>
                <div class='card equal-height'>
                    <div class='card-content'>
                        <!-- Place image inside .level within your card element -->
                        <nav class="level">
                            <div class="level-item has-text-centered">
                                <figure class='image is-128x128'>
                                    <img style="background-color: #EFEFEF;" class="is-rounded"
                                        src="images/{{$entrenador->avatar}}" alt="Placeholder image">
                                </figure>
                            </div>
                        </nav>
                        <nav class="level">
                            <div class="media-content has-text-centered">
                                <p class="title is-4">{{$entrenador->name}}</p>
                            </div>

                        </nav>
                        <nav class="level">
                            <div class="content has-text-centered">
                                {{$entrenador->description}}
                            </div>

                        </nav>
                        <!-- And it'll be centered like so -->
                    </div>

                    <footer class="card-footer">
                        <a href="/entrenadores/{{$entrenador->slug}}" class="card-footer-item">Ver mas...</a>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    @endforeach




</body>

</html>