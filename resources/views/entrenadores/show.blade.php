@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <title>entrenador</title>
</head>

<body>      
        @section('content')
        <div class="level-item has-text-centered">
        <figure class='image is-128x128'>
        
                                    <img style="background-color: #EFEFEF;" class="is-rounded"
                                        src="/images/{{$trainer->avatar}}" alt="Placeholder image">
                                </figure>
        </div>
   

        <div class="media-content has-text-centered">
            <p class="title is-4">{{$trainer->name}}</p>
        </div>

   
  
        <div class="has-text-centered">
             {{$trainer->description}}
             <br><br>        
            <a href="/entrenadores/{{$trainer->slug}}/edit" class="button is-link">Editar</a>
          
            <form action="/entrenadores/{{$trainer->slug}}" method="post">
            @method('DELETE')
            @csrf
            <button class="button">Eliminar</button>
            </form>
        </div>

        <add-pokemon-btn></add-pokemon-btn>
        <create-form-pokemon> </create-form-pokemon>
        <pokemons-component> </pokemons-component> 

        
        @endsection

</body>

</html>